﻿#if NET6_0_OR_GREATER
using ACE.Server.Network.GameMessages;
#endif
#if NET48_OR_GREATER
using UtilityBelt.Common.Enums;
#endif
using System.IO;

public class GameMessageCustomHealthChanged
#if NET6_0_OR_GREATER
    : GameMessage
#endif
    {
#if NET6_0_OR_GREATER
    public GameMessageCustomHealthChanged(uint targetId, uint attackerId, DamageType damageType, int healthChange, uint newHealth)
        : base((GameMessageOpcode)0xA001, GameMessageGroup.UIQueue) {
        Writer.Write(targetId);
        Writer.Write(attackerId);
        Writer.Write((int)damageType);
        Writer.Write(healthChange);
        Writer.Write(newHealth);
    }
#endif
#if NET48_OR_GREATER
    public static readonly MessageType Type = (MessageType)0xA001;

    public int TargetId { get; }
    public int AttackerId { get; }
    public DamageType DamageType { get; }
    public int HealthChange { get; }
    public uint NewHealth { get; }

    public GameMessageCustomHealthChanged(byte[] data) {
        using (var stream = new MemoryStream(data))
        using (var reader = new BinaryReader(stream)) {
            TargetId = reader.ReadInt32();
            AttackerId = reader.ReadInt32();
            DamageType = (DamageType)reader.ReadInt32();
            HealthChange = reader.ReadInt32();
            NewHealth = reader.ReadUInt32();
        }
    }
#endif
}