﻿using ACE.Database.Models.Auth;
using ACE.Entity.Enum;
using ACE.Server.Entity;
using ACE.Server.Network.GameMessages.Messages;
using ACE.Server.Network.Managers;
using ACE.Server.WorldObjects;
using static System.Net.Mime.MediaTypeNames;

namespace ACE.Mods.CustomMessageDemo {
    [HarmonyPatch]
    public class PatchClass {
        #region Settings
        const int RETRIES = 10;

        public Settings Settings = new();
        static string settingsPath => Path.Combine(Mod.ModPath, "Settings.json");
        private FileInfo settingsInfo = new(settingsPath);

        private JsonSerializerOptions _serializeOptions = new() {
            WriteIndented = true,
            AllowTrailingCommas = true,
            Converters = { new JsonStringEnumConverter(JsonNamingPolicy.CamelCase) },
            Encoder = JavaScriptEncoder.UnsafeRelaxedJsonEscaping,
        };

        private void SaveSettings() {
            string jsonString = JsonSerializer.Serialize(Settings, _serializeOptions);

            if (!settingsInfo.RetryWrite(jsonString, RETRIES)) {
                ModManager.Log($"Failed to save settings to {settingsPath}...", ModManager.LogLevel.Warn);
                Mod.State = ModState.Error;
            }
        }

        private void LoadSettings() {
            if (!settingsInfo.Exists) {
                ModManager.Log($"Creating {settingsInfo}...");
                SaveSettings();
            }
            else
                ModManager.Log($"Loading settings from {settingsPath}...");

            if (!settingsInfo.RetryRead(out string jsonString, RETRIES)) {
                Mod.State = ModState.Error;
                return;
            }

            try {
                Settings = JsonSerializer.Deserialize<Settings>(jsonString, _serializeOptions);
            }
            catch (Exception) {
                ModManager.Log($"Failed to deserialize Settings: {settingsPath}", ModManager.LogLevel.Warn);
                Mod.State = ModState.Error;
                return;
            }
        }
        #endregion

        #region Start/Shutdown
        public void Start() {
            //Need to decide on async use
            Mod.State = ModState.Loading;
            LoadSettings();

            if (Mod.State == ModState.Error) {
                ModManager.DisableModByPath(Mod.ModPath);
                return;
            }

            Mod.State = ModState.Running;
        }

        public void Shutdown() {
            //if (Mod.State == ModState.Running)
            // Shut down enabled mod...

            //If the mod is making changes that need to be saved use this and only manually edit settings when the patch is not active.
            //SaveSettings();

            if (Mod.State == ModState.Error)
                ModManager.Log($"Improper shutdown: {Mod.ModPath}", ModManager.LogLevel.Error);
        }
        #endregion

        #region Commands

        [CommandHandler("ptest", AccessLevel.Player, CommandHandlerFlag.RequiresWorld, 0, "Refunds costs associated with /raise.")]
        public static void HandleRaiseRefund(Session session, params string[] parameters) {
            var player = session.Player;

            var damage = (new Random()).Next(0, 1000);
            ChatPacket.SendServerMessage(session, $"Sending GameMessageCustomHealthChanged on/to current player, with random damage: {damage}", ChatMessageType.Broadcast);

            GameMessageCustomHealthChanged customHealthChangedMessage = new GameMessageCustomHealthChanged(player.Guid.Full, player.Guid.Full, DamageType.Undef, damage, player.Health.Current);
            session.Player.SendNetwork(customHealthChangedMessage, false);
        }
        #endregion

        #region Patches
        [HarmonyPostfix]
        [HarmonyPatch(typeof(DamageHistory), nameof(DamageHistory.Add), new Type[] { typeof(WorldObject), typeof(DamageType), typeof(uint) })]
        public static void PostAdd(WorldObject attacker, DamageType damageType, uint amount, ref DamageHistory __instance) {
            if (__instance.Creature is null)
                return;

            var creatureId = __instance.Creature.Guid.Full;
            var lastDamagerId = __instance.LastDamager?.Guid.Full ?? 0;
            var newHealth = __instance.Creature.Health.Current;
            var msg = new GameMessageCustomHealthChanged(creatureId, lastDamagerId, damageType, (int)-amount, newHealth);
            __instance.Creature.EnqueueBroadcast(msg);
        }

        [HarmonyPostfix]
        [HarmonyPatch(typeof(DamageHistory), nameof(DamageHistory.OnHeal), new Type[] { typeof(uint) })]
        public static void PostOnHeal(uint healAmount, ref DamageHistory __instance) {
            if (__instance.Creature is null)
                return;

            var creatureId = __instance.Creature.Guid.Full;
            var newHealth = __instance.Creature.Health.Current;
            // todo: how to get last healer.. assume it was yourself for now
            var msg = new GameMessageCustomHealthChanged(creatureId, creatureId, DamageType.Undef, (int)healAmount, newHealth);
            __instance.Creature.EnqueueBroadcast(msg);
        }


        #endregion
    }

}
